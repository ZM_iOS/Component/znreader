//
//  ZNPageViewController.swift
//  ZNReader
//
//  Created by Ink on 2018/11/11.
//

import UIKit

/// 翻页方式
public enum TransitionStyle : Int {
    /// 仿真
    case pageCurl
    /// 横向
    case horizontalScroll
    /// 纵向
    case verticalScroll
    /// 横向整页
    case horizontalPageScroll
    /// 纵向整页
    case verticalPageScroll
    /// 横覆盖
    case horizontalCover
    /// 纵覆盖
    case verticalCover
    /// 无动画
    case noneAnimation
    /// 无动画_不可拖拽
    case noneAnimation_dragForbid
    /// 自动
    case autoGradually
}

public protocol ZNPageViewControllerDataSource: NSObjectProtocol {
    func beforeView(in pageViewController: ZNPageViewController, view: UIView) -> UIView?
    func afterView(in pageViewController: ZNPageViewController, view: UIView) -> UIView?
    func backgroundView(in pageViewController: ZNPageViewController, for view: UIView) -> UIView?
}

public protocol ZNPageViewControllerDelegate: NSObjectProtocol {
    func currentView(in pageViewController: ZNPageViewController, didChangeTo view: UIView)
}

class ExtPageViewController : UIPageViewController {
        
    static let swizz: Void = {
        methodSwizzling()
    }()
    
    override func viewDidLoad() {
        ExtPageViewController.swizz
        super.viewDidLoad()
    }
    
    override func setViewControllers(_ viewControllers: [UIViewController]?, direction: UIPageViewController.NavigationDirection, animated: Bool, completion: ((Bool) -> Void)? = nil) {
        guard let viewControllers = viewControllers, viewControllers.count > 0 else { return }
        super.setViewControllers(viewControllers, direction: direction, animated: animated, completion: completion)
    }
}

extension ExtPageViewController {
    
    @objc static func methodSwizzling() -> Void {
        let originalSelector = Selector(("_setViewControllers:withCurlOfType:fromLocation:direction:animated:notifyDelegate:completion:"))
        let swizzSelector = #selector(ExtPageViewController.swizzled_setViewControllers(_:curlOfType:fromLocation:direction:animated:notifyDelegate:completion:))
        Swizzle(ExtPageViewController.self) {
            originalSelector <-> swizzSelector
        }
    }
    
    @objc private func swizzled_setViewControllers(_ viewControllers: [UIViewController]?, curlOfType:UIPageViewController.TransitionStyle,fromLocation:CGPoint, direction: UIPageViewController.NavigationDirection, animated: Bool, notifyDelegate:Bool, completion: ((Bool) -> Void)? = nil) {
        guard let viewControllers = viewControllers, viewControllers.count > 0 else { return }
        swizzled_setViewControllers(viewControllers,curlOfType:curlOfType,fromLocation:fromLocation, direction: direction, animated: animated, notifyDelegate:notifyDelegate, completion: completion)
    }
}

public class ZNPageViewController: UIViewController, ZNCircleScrollViewDataSource, ZNCircleScrollViewDelegate, UIPageViewControllerDataSource, UIPageViewControllerDelegate, UIGestureRecognizerDelegate, ZNCoverPageViewDataSource, ZNCoverPageViewDelegate, ZNGraduallyShowViewDataSource, ZNGraduallyShowViewDelegate {
    
    weak open var dataSource: ZNPageViewControllerDataSource?
    weak open var delegate: ZNPageViewControllerDelegate?
    public var transitionStyle: TransitionStyle {
        didSet {
            transitionStyleDidChange()
        }
    }
    
    private var _isDoubleSided: Bool = false
    public var isDoubleSided: Bool {
        get {
            return _isDoubleSided
        }
        set {
            _isDoubleSided = newValue
            _pageViewController?.isDoubleSided = newValue
        }
    }
    /*
    速度
    let types = [10,33,48,60,100] // 每分钟 N 行
    let lineHeight = (readerConfig.mainBody.font.pointSize + readerConfig.mainBody.style.lineSpacing) // 行高
    let speedType = types[(Int(arc4random()) % types.count)]
    pageVC.speed = TimeInterval(60.0 / (lineHeight * CGFloat(speedType)))
    **/
    public var speed: TimeInterval {
        get {
            return _graduallyView?.speed ?? 0
        }
        set {
            _graduallyView?.speed = newValue
        }
    }
    
    public var shownViews: [UIView] {
        switch transitionStyle {
        case .pageCurl:
            return pageVCShownViews
        case .horizontalScroll, .verticalScroll, .horizontalPageScroll, .verticalPageScroll:
            return scrollViewShownViews
        case .horizontalCover, .verticalCover,.noneAnimation,.noneAnimation_dragForbid:
            return coverViewShownViews
        case .autoGradually:
            return graduallyViewShownViews
        }
    }
    private var scrollViewShownViews: [UIView] {
        return _scrollView?.shownViews ?? [UIView]()
    }
    private var pageVCShownViews: [UIView] {
        var array = [UIView]()
        pageViewController?.viewControllers?.forEach({
            if let vc = $0 as? ZNContentViewController {
                array.append(vc.contentView)
            }
        })
        return array
    }
    private var coverViewShownViews: [UIView] {
        return _coverView?.shownViews ?? [UIView]()
    }
    private var graduallyViewShownViews: [UIView] {
        return _graduallyView?.shownViews ?? [UIView]()
    }
    
    
    private var _scrollView: ZNCircleScrollView?
    private var scrollView: ZNCircleScrollView? {
        if _scrollView == nil {
            _scrollView = ZNCircleScrollView.init(frame: self.view.bounds)
        }
        return _scrollView
    }
    
    private var _pageViewController: ExtPageViewController?
    private var pageViewController: ExtPageViewController? {
        if _pageViewController == nil {
            _pageViewController = ExtPageViewController.init(transitionStyle: .pageCurl, navigationOrientation: .horizontal, options: [UIPageViewController.OptionsKey.spineLocation: NSNumber(value:UIPageViewController.SpineLocation.min.rawValue)])
            _pageViewController?.view.frame = view.bounds
            _pageViewController?.isDoubleSided = _isDoubleSided
            _pageViewController?.gestureRecognizers.forEach({
                if $0 is UITapGestureRecognizer {
                    $0.isEnabled = false
                }
            })
        }
        return _pageViewController
    }
    
    private var _coverView: ZNCoverPageView?
    private var coverView: ZNCoverPageView? {
        if _coverView == nil {
            _coverView = ZNCoverPageView.init(frame: self.view.bounds)
        }
        return _coverView
    }
    
    private var _graduallyView: ZNGraduallyShowView?
    private var graduallyView: ZNGraduallyShowView? {
        if _graduallyView == nil {
            _graduallyView = ZNGraduallyShowView.init(frame: self.view.bounds)
        }
        return _graduallyView
    }
    
    // MARK: public method
    
    public init(transitionStyle: TransitionStyle) {
        self.transitionStyle = transitionStyle
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func setView(view: UIView, direction: Direction, animated: Bool) {
        view.frame = CGRect.init(origin: CGPoint.zero, size: self.view.bounds.size)
        switch transitionStyle {
        case .pageCurl:
            guard let pageViewController = pageViewController else {
                return
            }
            let vc = ZNContentViewController.init(contentView: view, viewSize: pageViewController.view.bounds.size, isBackground: false)
            if vc.available {
                var array = [vc]
                if pageViewController.isDoubleSided && animated {
                    switch direction {
                    case .forward:
                        if let currentView = self.shownViews.last {
                            if let bgView = dataSource?.backgroundView(in: self, for: currentView) {
                                let bgVC = ZNContentViewController.init(contentView: bgView, viewSize: pageViewController.view.bounds.size, isBackground: true)
                                array.append(bgVC)
                            }
                        }
                    case .reverse:
                        if let bgView = dataSource?.backgroundView(in: self, for: view) {
                            let bgVC = ZNContentViewController.init(contentView: bgView, viewSize: pageViewController.view.bounds.size, isBackground: true)
                            array.append(bgVC)
                        }
                    }
                }
                pageViewController.setViewControllers(array, direction: UIPageViewController.NavigationDirection.init(rawValue: direction.rawValue) ?? .forward, animated: animated, completion: nil)
                delegate?.currentView(in: self, didChangeTo: view)
            }
        case .horizontalScroll, .verticalScroll, .horizontalPageScroll, .verticalPageScroll:
            scrollView?.setView(view: view, direction: direction, animated: animated)
        case .verticalCover, .horizontalCover:
            coverView?.setView(view: view, direction: direction, animated: animated)
        case .noneAnimation,.noneAnimation_dragForbid:
            coverView?.setView(view: view, direction: direction, animated: false)
        case .autoGradually:
            graduallyView?.setView(view: view, direction: direction, animated: animated)
        }
    }
    
    //    override init
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        automaticallyAdjustsScrollViewInsets = false
        transitionStyleDidChange()
    }
    
    // MARK: private method
    
    private func transitionStyleDidChange () {
        removeUnusable()
        switch transitionStyle {
        case .pageCurl:
            guard let pageViewController = pageViewController else{
                return
            }
            addChild(pageViewController)
            view.addSubview(pageViewController.view)
            pageViewController.dataSource = self
            pageViewController.delegate = self
            pageViewController.isDoubleSided = _isDoubleSided
        case .horizontalScroll, .verticalScroll, .horizontalPageScroll, .verticalPageScroll:
            guard let scrollView = scrollView else{
                return
            }
            view.addSubview(scrollView)
            scrollView.scrollDirection = (transitionStyle == .horizontalScroll || transitionStyle == .horizontalPageScroll) ? .horizontal : .vertical
            scrollView.isPagingEnabled = (transitionStyle == .horizontalPageScroll || transitionStyle == .verticalPageScroll)
            scrollView.dataSource = self
            scrollView.delegate = self
        case .verticalCover, .horizontalCover,.noneAnimation,.noneAnimation_dragForbid:
            guard let coverView = coverView else{
                return
            }
            view.addSubview(coverView)
            coverView.scrollDirection = (transitionStyle == .horizontalCover || transitionStyle == .noneAnimation || transitionStyle == .noneAnimation_dragForbid) ? .horizontal : .vertical
            coverView.dragForbid = transitionStyle == .noneAnimation_dragForbid
            coverView.dataSource = self
            coverView.delegate = self
        case .autoGradually:
            guard let graduallyView = graduallyView else{
                return
            }
            view.addSubview(graduallyView)
            graduallyView.scrollDirection = .vertical
            graduallyView.dataSource = self
            graduallyView.delegate = self
        }
    }
    
    func removeUnusable() {
        removePageVC()
        removeScrollView()
        removeCoverView()
        removeGraduallyView()
    }
    
    private func removeScrollView() {
        if _scrollView != nil {
            _scrollView?.dataSource = nil
            _scrollView?.delegate = nil
            _scrollView?.removeFromSuperview()
            _scrollView = nil
        }
    }
    
    private func removePageVC() {
        if _pageViewController != nil {
            _pageViewController?.delegate = nil
            _pageViewController?.dataSource = nil
            _pageViewController?.view.removeFromSuperview()
            _pageViewController?.removeFromParent()
            _pageViewController = nil
        }
    }
    
    private func removeCoverView() {
        if _coverView != nil {
            _coverView?.dataSource = nil
            _coverView?.delegate = nil
            _coverView?.removeFromSuperview()
            _coverView = nil
        }
    }
    
    private func removeGraduallyView() {
        if _graduallyView != nil {
            _graduallyView?.dataSource = nil
            _graduallyView?.delegate = nil
            _graduallyView?.removeFromSuperview()
            _graduallyView = nil
        }
    }
    
    // MARK: ZNCoverPageViewDataSource
    
    public func beforeView(in coverView: ZNCoverPageView, centerView: UIView) -> UIView? {
        return dataSource?.beforeView(in: self, view: centerView)
    }
    
    public func afterView(in coverView: ZNCoverPageView, centerView: UIView) -> UIView? {
        return dataSource?.afterView(in: self, view: centerView)
    }
    
    public func centerViewDidChanged(in coverView: ZNCoverPageView, centerView: UIView?) {
        if let centerView = centerView {
            delegate?.currentView(in: self, didChangeTo: centerView)
        }
    }
    
    // MARK: ZNCircleScrollViewDataSource
    
    public func beforeView(in scrollView: ZNCircleScrollView, centerView: UIView) -> UIView? {
        return dataSource?.beforeView(in: self, view: centerView)
    }
    
    public func afterView(in scrollView: ZNCircleScrollView, centerView: UIView) -> UIView? {
        return dataSource?.afterView(in: self, view: centerView)
    }
    
    public func centerViewDidChanged(in scrollView: ZNCircleScrollView, centerView: UIView?) {
        if let centerView = centerView {
            delegate?.currentView(in: self, didChangeTo: centerView)
        }
    }
    
    // MARK: ZNGraduallyShowViewDataSource
    
    public func beforeView(in coverView: ZNGraduallyShowView, centerView: UIView) -> UIView? {
        return dataSource?.beforeView(in: self, view: centerView)
    }
    
    public func afterView(in coverView: ZNGraduallyShowView, centerView: UIView) -> UIView? {
        return dataSource?.afterView(in: self, view: centerView)
    }
    
    public func centerViewDidChanged(in coverView: ZNGraduallyShowView, centerView: UIView?) {
        if let centerView = centerView {
            delegate?.currentView(in: self, didChangeTo: centerView)
        }
    }
    
    // MARK: UIPageViewControllerDataSource
    
    var beforeViewCache: UIView?
    var afterViewCache: UIView?
    
    public func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let currentVC = viewController as? ZNContentViewController else {
            return nil
        }
        afterViewCache = nil
        if pageViewController.isDoubleSided {
            if !currentVC.available {
                return nil
            }
            if beforeViewCache == nil {
                beforeViewCache = dataSource?.beforeView(in: self, view: currentVC.contentView)
            }
            if beforeViewCache == nil {
                return nil
            }
            
            if currentVC.isBackground {
                let vc = ZNContentViewController.init(contentView: beforeViewCache, viewSize: pageViewController.view.bounds.size, isBackground: false)
                if vc.available {
                    beforeViewCache = nil
                    return vc
                }
                beforeViewCache = nil
                return nil
            }
            else {
                if let beforeViewCache = beforeViewCache,let bgView = dataSource?.backgroundView(in: self, for: beforeViewCache) {
                    let vc = ZNContentViewController.init(contentView: bgView, viewSize: pageViewController.view.bounds.size, isBackground: true)
                    if vc.available {
                        return vc
                    }
                }
                return nil
            }
        }
        else {
            if !currentVC.available {
                return nil
            }
            if let beforeView = dataSource?.beforeView(in: self, view: currentVC.contentView) {
                let vc = ZNContentViewController.init(contentView: beforeView, viewSize: pageViewController.view.bounds.size, isBackground: false)
                if vc.available {
                    return vc
                }
            }
        }
        return nil
    }
    
    public func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let currentVC = viewController as? ZNContentViewController else {
            return nil
        }
        beforeViewCache = nil
        if pageViewController.isDoubleSided {
            if !currentVC.available {
                return nil
            }
            if afterViewCache == nil {
                afterViewCache = dataSource?.afterView(in: self, view: currentVC.contentView)
            }
            if afterViewCache == nil {
                return nil
            }
            if currentVC.isBackground {
                let vc = ZNContentViewController.init(contentView: afterViewCache, viewSize: pageViewController.view.bounds.size, isBackground: false)
                if vc.available {
                    afterViewCache = nil
                    return vc
                }
                afterViewCache = nil
                return nil
            }
            else {
                if let bgView = dataSource?.backgroundView(in: self, for: currentVC.contentView) {
                    let vc = ZNContentViewController.init(contentView: bgView, viewSize: pageViewController.view.bounds.size, isBackground: true)
                    if vc.available {
                        return vc
                    }
                }
                return nil
            }
        }
        else {
            if !currentVC.available {
                return nil
            }
            if let afterView = dataSource?.afterView(in: self, view: currentVC.contentView) {
                let vc = ZNContentViewController.init(contentView: afterView, viewSize: pageViewController.view.bounds.size, isBackground: false)
                if vc.available {
                    return vc
                }
            }
        }
        return nil
    }
    
    public func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        beforeViewCache = nil
        afterViewCache = nil
        if finished, let currentVC = pageViewController.viewControllers?.last as? ZNContentViewController {
            delegate?.currentView(in: self, didChangeTo: currentVC.contentView)
        }
    }
    
}
