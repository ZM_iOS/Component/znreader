//
//  ZNCoverPageView.swift
//  Demo
//
//  Created by Ink on 2019/1/7.
//  Copyright © 2019 ncnk. All rights reserved.
//

import UIKit

public protocol ZNCoverPageViewDataSource: NSObjectProtocol {
    func beforeView(in coverView: ZNCoverPageView, centerView: UIView) -> UIView?
    func afterView(in coverView: ZNCoverPageView, centerView: UIView) -> UIView?
}

public protocol ZNCoverPageViewDelegate: NSObjectProtocol {
    func centerViewDidChanged(in coverView: ZNCoverPageView, centerView: UIView?)
}

public class ZNCoverPageView: UIView, UIGestureRecognizerDelegate {
    
    weak open var dataSource: ZNCoverPageViewDataSource?
    weak open var delegate: ZNCoverPageViewDelegate?

    public var scrollDirection: ScrollDirection {
        didSet {
            resetContentView()
        }
    }
    
    public var shownViews = [UIView]() {
        didSet {
            if shownViews.count > 2 {
                shownViews.removeFirst().removeFromSuperview()
            }
        }
    }
    
    private func shownViewsRemove(_ item : UIView?) {
        guard let item = item else {
            return
        }
        item.removeFromSuperview()
        shownViews = shownViews.filter({$0 != item})
    }
    
    private var centerView: UIView? {
        didSet {
            if centerView != oldValue {
                delegate?.centerViewDidChanged(in: self, centerView: centerView)
            }
        }
    }
    
    private var animationView: UIView?
    private var panGestureRecognizer: UIPanGestureRecognizer = UIPanGestureRecognizer.init()
    
    
    // MARK: - public
    
    public func setView(view: UIView, direction: Direction, animated: Bool) {
        resetView(view: view, direction: direction, animated: animated)
    }
    
    // MARK: -
    
    required init?(coder aDecoder: NSCoder) {
        scrollDirection = .horizontal
        super.init(coder: aDecoder)
        addGesture()
    }
    
    public override init(frame: CGRect) {
        scrollDirection = .horizontal
        super.init(frame: frame)
        addGesture()
    }
    
    func addGesture() {
        panGestureRecognizer.addTarget(self, action: #selector(panGestureRecognizerAction(panGestureRecognizer:)))
        panGestureRecognizer.delegate = self
        addGestureRecognizer(panGestureRecognizer)
    }
    
    public var dragForbid = false {
        didSet {
            panGestureRecognizer.isEnabled = !dragForbid
        }
    }
    
    // MARK: - Gesture
    
    private var startPoint: CGPoint = .zero
    private var panDirection: Direction = .forward
    private var ifDealGesture = true
    
    public override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return centerView != nil
    }
    
    @objc func panGestureRecognizerAction(panGestureRecognizer: UIPanGestureRecognizer) {
        if ifDealGesture == false {
            return
        }
        let currentPoint = panGestureRecognizer.location(in: self)
        switch panGestureRecognizer.state {
        case .began:
            startPoint = currentPoint
        case .changed:
            scroll(currentPoint: currentPoint)
        case .ended, .cancelled, .failed:
            panGestureDidEnd(currentPoint: currentPoint)
        default: break
        }
    }
    
    private func scroll(currentPoint: CGPoint) {
                    
        switch scrollDirection {
        case .horizontal:
            if abs(currentPoint.x - startPoint.x) < 10, let _ = animationView {  break }
            panDirection = currentPoint.x < startPoint.x ? .forward : .reverse
            startPoint.x = panDirection == .forward ? min(currentPoint.x, startPoint.x) : max(currentPoint.x, startPoint.x)
            
        case .vertical:
            if abs(currentPoint.y - startPoint.y) < 10, let _ = animationView {  break }
            panDirection = currentPoint.y <= startPoint.y ? .forward : .reverse
            startPoint.y = panDirection == .forward ? min(currentPoint.y, startPoint.y) : max(currentPoint.y, startPoint.y)
        }
        
        if animationView == nil, let centerView = centerView {
            let view = panDirection == .forward ? dataSource?.afterView(in: self, centerView: centerView) : dataSource?.beforeView(in: self, centerView: centerView)
            coverInsert(view: view , direction: panDirection)
        }
        
        if let animationView = animationView {
            let newPoint = scrollDirection == .horizontal ? CGPoint.init(x: currentPoint.x - bounds.size.width, y: 0) : CGPoint.init(x: 0, y: currentPoint.y - bounds.size.height)
            UIView.animate(withDuration: 0.1) {
                animationView.frame = CGRect.init(origin: newPoint, size: self.bounds.size)
            }
        }
    }
    
    private func panGestureDidEnd(currentPoint: CGPoint) {
        
        switch scrollDirection {
        case .vertical:
            animationViewComplation(CGPoint(x: 0, y: panDirection == .forward ? -bounds.size.height : 0), panGestureEnd: true)
        case .horizontal:
            animationViewComplation(CGPoint(x: panDirection == .forward ? -bounds.size.width : 0, y: 0), panGestureEnd: true)
        }
    }
    
    private func animationViewComplation(_ origin: CGPoint, panGestureEnd: Bool = false) {
        ifDealGesture = false
        guard let _ = animationView  else {
            ifDealGesture = true
            return
        }
        
        UIView.animate(withDuration: panGestureEnd ? 0.15 : 0.25, animations: {
            self.animationView?.frame = CGRect(origin:origin, size: self.bounds.size)
        }) {[weak self] finish in
            if origin != .zero {
                self?.shownViewsRemove(self?.animationView)
            }
            self?.centerView = self?.shownViews.last
            self?.animationView = nil
            self?.ifDealGesture = true
        }
    }
    
    private func coverInsert(view: UIView? = nil, direction: Direction) {
        
        guard let view = view, let centerView = centerView else { return }
        
        switch direction {
        case .forward:
            insertSubview(view, belowSubview: centerView)
            shownViews.insert(view, at: shownViews.count - 1)
            centerView.frame = bounds
            animationView = centerView
        case .reverse:
            insertSubview(view, aboveSubview: centerView)
            shownViews.append(view)
            let origin = scrollDirection == .vertical ? CGPoint(x: 0, y: -bounds.size.height) : CGPoint(x: -bounds.size.width, y: 0)
            view.frame = CGRect.init(origin: origin, size: bounds.size)
            animationView = view
        }
    }
    
    // MARK: - private method
    
    public override var frame: CGRect {
        didSet {
            resetContentView()
        }
    }
    
    private func resetContentView () {
        if let centerView = centerView {
            setView(view: centerView, direction: .forward, animated: false)
        }
    }
    
    private func resetView(view: UIView, direction: Direction, animated: Bool) {
        
        if animated , let centerView = centerView , centerView != view {
                        
            if animationView == nil {
                coverInsert(view: view, direction: direction)
            }
            
            let origin = scrollDirection == .vertical ? CGPoint(x: 0, y: -bounds.size.height) : CGPoint(x: -bounds.size.width, y: 0)
            animationViewComplation(direction == .forward ? origin : .zero)
        }
        else {
            setView(view: view)
        }
    }
    
    private func setView(view: UIView) {
        if animationView != nil {
            shownViewsRemove(animationView)
            animationView = nil
        }
        if centerView != nil && centerView != view {
            shownViewsRemove(centerView)
            centerView = nil
        }
        view.frame = bounds
        addSubview(view)
        centerView = view
        shownViews.append(view)
    }

}
