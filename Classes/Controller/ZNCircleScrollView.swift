//
//  ZNReaderScrollView.swift
//  ZNReader
//
//  Created by Ink on 2018/11/10.
//

import UIKit

public enum Direction : Int {
    case forward
    case reverse
}

public enum ScrollDirection : Int {
    case horizontal
    case vertical
}

public protocol ZNCircleScrollViewDataSource: NSObjectProtocol {
    func beforeView(in scrollView: ZNCircleScrollView, centerView: UIView) -> UIView?
    func afterView(in scrollView: ZNCircleScrollView, centerView: UIView) -> UIView?
}

public protocol ZNCircleScrollViewDelegate: NSObjectProtocol {
    func centerViewDidChanged(in scrollView: ZNCircleScrollView, centerView: UIView?)
}

public class ZNCircleScrollView: UIView, UIScrollViewDelegate {

    weak open var dataSource: ZNCircleScrollViewDataSource?
    weak open var delegate: ZNCircleScrollViewDelegate?
    public var scrollDirection: ScrollDirection {
        didSet {
            resetScrollView()
        }
    }
    public var isPagingEnabled: Bool {
        get {
            return scrollView.isPagingEnabled
        }
        set {
            scrollView.isPagingEnabled = newValue
        }
    }
    public var shownViews: [UIView] {
        get {
            var array = [UIView]()
            if let centerView = self.centerView {
                array.append(centerView)
            }
            if let reuseView = self.reuseView , reuseView != self.centerView {
                array.append(reuseView)
            }
            if array.count == 2 && array[1].frame.origin.x < array[0].frame.origin.x {
                array.swapAt(0, 1)
            }
            return array
        }
    }

    private var scrollView: UIScrollView = UIScrollView.init()
    private var _centerView: UIView?
    private var centerView: UIView? {
        get {
            return _centerView
        }
        set {
            if _centerView != newValue {
                _centerView = newValue
                delegate?.centerViewDidChanged(in: self, centerView: _centerView)
            }
        }
    }
    private var reuseView: UIView?
    private var centerViewFrame: CGRect = CGRect.zero

    
    // MARK: - public
    
    public func setView(view: UIView, direction: Direction, animated: Bool) {
        resetView(view: view, direction: direction, animated: animated)
    }
    
    // MARK: -

    required init?(coder aDecoder: NSCoder) {
        scrollDirection = .horizontal
        super.init(coder: aDecoder)
    }

    public override init(frame: CGRect) {
        scrollDirection = .horizontal
        super.init(frame: frame)
        
        addSubview(scrollView)
        scrollView.frame = bounds
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
        if #available(iOS 11.0, *) {
            scrollView.contentInsetAdjustmentBehavior = .never
        } else {
            // Fallback on earlier versions
        }
    }

    // MARK: - private method
    
    public override var frame: CGRect {
        didSet {
            scrollView.frame = bounds
        }
    }
    
    private func resetScrollView () {
        scrollView.delegate = nil
        if scrollDirection == .horizontal {
            scrollView.contentSize = CGSize.init(width: bounds.size.width*3, height: bounds.size.height)
            centerViewFrame = CGRect.init(origin: CGPoint.init(x: bounds.size.width, y: 0), size: bounds.size)
            scrollView.contentOffset = CGPoint.init(x: bounds.size.width, y: 0)
        }
        else if scrollDirection == .vertical {
            scrollView.contentSize = CGSize.init(width: bounds.size.width, height: bounds.size.height*3)
            centerViewFrame = CGRect.init(origin: CGPoint.init(x: 0, y: bounds.size.height), size: bounds.size)
            scrollView.contentOffset = CGPoint.init(x: 0, y: bounds.size.height)
        }
        
        if let centerView = centerView {
            setView(view: centerView, direction: .forward, animated: false)
        }
    }

    private func resetView(view: UIView, direction: Direction, animated: Bool) {
        if animated , let  centerView = centerView {
            switch direction {
            case .forward:
                switch scrollDirection {
                case .horizontal:
                    scrollView.delegate = nil
                    var lastView = centerView
                    if let reuseView = reuseView, reuseView.frame.origin.x > centerView.frame.origin.x {
                        lastView = reuseView
                    }
                    view.frame = CGRect.init(origin: CGPoint.init(x: lastView.frame.origin.x + scrollView.bounds.size.width, y: lastView.frame.origin.y), size: scrollView.bounds.size)
                    scrollView.addSubview(view)
                    scrollView.contentSize = CGSize.init(width: view.frame.origin.x+view.frame.size.width, height: scrollView.bounds.size.height)
                    UIView.animate(withDuration: 0.25, animations: {
                        self.scrollView.contentOffset = CGPoint.init(x: view.frame.origin.x, y: 0)
                    }) { (completion) in
                        if self.reuseView != nil {
                            self.reuseView?.removeFromSuperview()
                            self.reuseView = nil
                        }
                        if self.centerView != nil && self.centerView != view {
                            self.centerView?.removeFromSuperview()
                            self.centerView = nil
                        }
                        self.centerView = view
                        self.resetScrollView()
                    }
                case .vertical:
                    scrollView.delegate = nil
                    var lastView = centerView
                    if let reuseView = reuseView, reuseView.frame.origin.y > centerView.frame.origin.y {
                        lastView = reuseView
                    }
                    view.frame = CGRect.init(origin: CGPoint.init(x: lastView.frame.origin.x, y: lastView.frame.origin.y+scrollView.bounds.size.height), size: scrollView.bounds.size)
                    scrollView.addSubview(view)
                    scrollView.contentSize = CGSize.init(width: view.frame.origin.x, height: view.frame.origin.y+scrollView.bounds.size.height)
                    UIView.animate(withDuration: 0.25, animations: {
                        self.scrollView.contentOffset = CGPoint.init(x: 0, y: view.frame.origin.y)
                    }) { (completion) in
                        if self.reuseView != nil {
                            self.reuseView?.removeFromSuperview()
                            self.reuseView = nil
                        }
                        if self.centerView != nil && self.centerView != view {
                            self.centerView?.removeFromSuperview()
                            self.centerView = nil
                        }
                        self.centerView = view
                        self.resetScrollView()
                    }
                }
            case .reverse:
                switch scrollDirection {
                case .horizontal:
                    scrollView.delegate = nil
                    var firstView = centerView
                    if let reuseView = reuseView, reuseView.frame.origin.x < centerView.frame.origin.x {
                        firstView = reuseView
                    }
                    view.frame = CGRect.init(origin: CGPoint.init(x: firstView.frame.origin.x-scrollView.bounds.size.width, y: firstView.frame.origin.y), size: scrollView.bounds.size)
                    scrollView.addSubview(view)
                    UIView.animate(withDuration: 0.25, animations: {
                        self.scrollView.contentOffset = CGPoint.init(x: view.frame.origin.x, y: 0)
                    }) { (completion) in
                        if self.reuseView != nil {
                            self.reuseView?.removeFromSuperview()
                            self.reuseView = nil
                        }
                        if self.centerView != nil && self.centerView != view {
                            self.centerView?.removeFromSuperview()
                            self.centerView = nil
                        }
                        self.centerView = view
                        self.resetScrollView()
                    }
                case .vertical:
                    scrollView.delegate = nil
                    var firstView = centerView
                    if let reuseView = reuseView, reuseView.frame.origin.y < centerView.frame.origin.y {
                        firstView = reuseView
                    }
                    view.frame = CGRect.init(origin: CGPoint.init(x: firstView.frame.origin.x, y: firstView.frame.origin.y-scrollView.bounds.size.height), size: scrollView.bounds.size)
                    scrollView.addSubview(view)
                    UIView.animate(withDuration: 0.25, animations: {
                        self.scrollView.contentOffset = CGPoint.init(x: 0, y: view.frame.origin.y)
                    }) { (completion) in
                        if self.reuseView != nil {
                            self.reuseView?.removeFromSuperview()
                            self.reuseView = nil
                        }
                        if self.centerView != nil && self.centerView != view {
                            self.centerView?.removeFromSuperview()
                            self.centerView = nil
                        }
                        self.centerView = view
                        self.resetScrollView()
                    }
                }
            }
        }
        else {
            setView(view: view)
        }
    }
    
    private func setView(view: UIView) {
        scrollView.delegate = nil
        if reuseView != nil {
            reuseView?.removeFromSuperview()
            reuseView = nil
        }
        if centerView != nil && centerView != view {
            centerView?.removeFromSuperview()
            centerView = nil
        }
        scrollView.contentOffset = centerViewFrame.origin
        view.frame = centerViewFrame
        centerView = view
        scrollView.addSubview(view)
        scrollView.delegate = self
    }
    
    // MARK: - UIScrollView delegate

    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if centerView == nil {
            scrollView.delegate = nil
            scrollView.contentOffset = centerViewFrame.origin
            return
        }
        if scrollDirection == .horizontal {
            horizontalScrollViewDidScroll()
        }
        else if scrollDirection == .vertical {
            verticalScrollViewDidScroll()
        }
    }

    private func horizontalScrollViewDidScroll () {
        
        guard let centerView = centerView else {
            return
        }
        
        let offsetValue = scrollView.contentOffset.x
        let boundsValue = scrollView.bounds.width
        if offsetValue > boundsValue {
            if let reuseView = reuseView, reuseView.frame.origin.x < centerView.frame.origin.x {
                reuseView.removeFromSuperview()
                self.reuseView = nil
            }
            if offsetValue < boundsValue*2 {
                if reuseView == nil {
                    if let view = dataSource?.afterView(in: self, centerView: centerView) {
                        scrollView.addSubview(view)
                        view.frame = CGRect.init(origin: CGPoint.init(x: scrollView.bounds.size.width*2, y: 0), size: scrollView.bounds.size)
                        reuseView = view
                    }
                    else {
                        scrollView.setContentOffset(centerViewFrame.origin, animated: false)
                    }
                }
            }
            else {
                if reuseView != nil {
                    scrollView.contentOffset = CGPoint.init(x: offsetValue - boundsValue, y: 0)
                    centerView.removeFromSuperview()
                    self.centerView = reuseView
                    self.centerView?.frame = CGRect.init(origin: CGPoint.init(x: scrollView.bounds.size.width, y: 0), size: scrollView.bounds.size)
                    self.reuseView = nil
                }
                else {
                    scrollView.setContentOffset(centerViewFrame.origin, animated: false)
                }
            }
        }
        else if offsetValue < boundsValue {
            if offsetValue > 0 {
                if let reuseView = reuseView, reuseView.frame.origin.x > centerView.frame.origin.x {
                    reuseView.removeFromSuperview()
                    self.reuseView = nil
                }
                if reuseView == nil {
                    if let view = dataSource?.beforeView(in: self, centerView: centerView) {
                        scrollView.addSubview(view)
                        view.frame = CGRect.init(origin: CGPoint.init(x: 0, y: 0), size: scrollView.bounds.size)
                        reuseView = view
                    }
                    else {
                        scrollView.setContentOffset(centerViewFrame.origin, animated: false)
                    }
                }
            }
            else {
                if reuseView != nil {
                    scrollView.contentOffset = CGPoint.init(x: offsetValue + boundsValue, y: 0)
                    centerView.removeFromSuperview()
                    self.centerView = reuseView
                    self.centerView?.frame = CGRect.init(origin: CGPoint.init(x: scrollView.bounds.size.width, y: 0), size: scrollView.bounds.size)
                    reuseView = nil
                }
                else {
                    scrollView.setContentOffset(centerViewFrame.origin, animated: false)
                }
            }
        }
    }
    
    private func verticalScrollViewDidScroll () {
        
        guard let centerView = centerView else {
            return
        }
        
        let offsetValue = scrollView.contentOffset.y
        let boundsValue = scrollView.bounds.height
        if offsetValue > boundsValue {
            if let reuseView = reuseView, reuseView.frame.origin.y < centerView.frame.origin.y {
                reuseView.removeFromSuperview()
                self.reuseView = nil
            }
            if offsetValue < boundsValue*2 {
                if reuseView == nil {
                    if let view = dataSource?.afterView(in: self, centerView: centerView) {
                        scrollView.addSubview(view)
                        view.frame = CGRect.init(origin: CGPoint.init(x: 0, y: scrollView.bounds.size.height*2), size: scrollView.bounds.size)
                        reuseView = view
                    }
                    else {
                        scrollView.setContentOffset(centerViewFrame.origin, animated: false)
                    }
                }
            }
            else {
                if reuseView != nil {
                    scrollView.contentOffset = CGPoint.init(x: 0, y: offsetValue - boundsValue)
                    centerView.removeFromSuperview()
                    self.centerView = reuseView
                    self.centerView?.frame = CGRect.init(origin: CGPoint.init(x: 0, y: scrollView.bounds.size.height), size: scrollView.bounds.size)
                    reuseView = nil
                }
                else {
                    scrollView.setContentOffset(centerViewFrame.origin, animated: false)
                }
            }
        }
        else if offsetValue < boundsValue {
            if offsetValue > 0 {
                if let reuseView = reuseView, reuseView.frame.origin.y > centerView.frame.origin.y {
                    reuseView.removeFromSuperview()
                    self.reuseView = nil
                }
                if reuseView == nil {
                    if let view = dataSource?.beforeView(in: self, centerView: centerView) {
                        scrollView.addSubview(view)
                        view.frame = CGRect.init(origin: CGPoint.init(x: 0, y: 0), size: scrollView.bounds.size)
                        reuseView = view
                    }
                    else {
                        scrollView.setContentOffset(centerViewFrame.origin, animated: false)
                    }
                }
            }
            else {
                if reuseView != nil {
                    scrollView.contentOffset = CGPoint.init(x: 0, y: offsetValue + boundsValue)
                    centerView.removeFromSuperview()
                    self.centerView = reuseView
                    self.centerView?.frame = CGRect.init(origin: CGPoint.init(x: 0, y: scrollView.bounds.size.height), size: scrollView.bounds.size)
                    reuseView = nil
                }
                else {
                    scrollView.setContentOffset(centerViewFrame.origin, animated: false)
                }
            }
        }
    }
    
}
