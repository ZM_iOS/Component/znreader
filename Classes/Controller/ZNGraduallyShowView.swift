//
//  ZNGraduallyShowView.swift
//  Demo
//
//  Created by Ink on 2019/1/8.
//  Copyright © 2019 ncnk. All rights reserved.
//

import UIKit

public protocol ZNGraduallyShowViewDataSource: NSObjectProtocol {
    func beforeView(in coverView: ZNGraduallyShowView, centerView: UIView) -> UIView?
    func afterView(in coverView: ZNGraduallyShowView, centerView: UIView) -> UIView?
}

public protocol ZNGraduallyShowViewDelegate: NSObjectProtocol {
    func centerViewDidChanged(in coverView: ZNGraduallyShowView, centerView: UIView?)
}

public class ZNGraduallyShowView: UIView, UIGestureRecognizerDelegate {

    weak open var dataSource: ZNGraduallyShowViewDataSource?
    weak open var delegate: ZNGraduallyShowViewDelegate?
    private var direction: Direction = .forward
    weak var timer: Timer?
    public var scrollDirection: ScrollDirection {
        didSet {
            if let centerView = centerView {
                setView(view: centerView)
            }
        }
    }
    public var shownViews: [UIView] {
        get {
            var array = [UIView]()
            if let reuseView = reuseView, reuseView != centerView {
                array.append(reuseView)
            }
            if let centerView = centerView {
                array.append(centerView)
            }
            return array
        }
    }
    
    private var _centerView: UIView?
    private var centerView: UIView? {
        get {
            return _centerView
        }
        set {
            if _centerView != newValue {
                _centerView = newValue
                delegate?.centerViewDidChanged(in: self, centerView: _centerView)
            }
        }
    }
    lazy var shadowView: UIView = {
        let view = UIView()
        view.clipsToBounds = false
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.lightGray
        view.layer.shadowColor = UIColor.lightGray.cgColor
        view.layer.shadowRadius = 3
        view.layer.shadowOffset = CGSize.init(width: 0, height: -3)
        view.layer.shadowOpacity = 1.0
        return view
    }()

    private var reuseView: UIView?
    private var centerViewFrame: CGRect = CGRect.zero
    private var panGestureRecognizer: UIPanGestureRecognizer = UIPanGestureRecognizer.init()
    
    private var _speed: TimeInterval = 0.0
    public var speed: TimeInterval {
        get {
            return _speed
        }
        set {
            _speed = newValue
            pauseAnimation()
            if _speed != 0 {
                if let _ = reuseView {
                    startAnimation()
                }
                else {
                    autoShowNextView()
                }
            }
        }
    }
    
    // MARK: - public
    
    public func setView(view: UIView, direction: Direction, animated: Bool) {
        setView(view: view)
    }
    
    // MARK: - init
    
    required init?(coder aDecoder: NSCoder) {
        scrollDirection = .horizontal
        super.init(coder: aDecoder)
        addGesture()
    }
    
    public override init(frame: CGRect) {
        scrollDirection = .horizontal
        super.init(frame: frame)
        addGesture()
    }
    
    public override var frame: CGRect {
        didSet {
            if let centerView = centerView {
                setView(view: centerView)
            }
        }
    }
    
    // MARK: - Gesture
    
    private func addGesture() {
        self.panGestureRecognizer.addTarget(self, action: #selector(panGestureRecognizerAction(panGestureRecognizer:)))
        self.panGestureRecognizer.delegate = self
        self.addGestureRecognizer(self.panGestureRecognizer)
    }
    
    public override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return self.centerView != nil
    }
    
    @objc func panGestureRecognizerAction(panGestureRecognizer: UIPanGestureRecognizer) {
        switch panGestureRecognizer.state {
        case .changed:
            if let _ = reuseView {
                pauseAnimation()
                let translation = panGestureRecognizer.translation(in: self)
                updateReuseViewFrame(translationY: translation.y)
                panGestureRecognizer.setTranslation(.zero, in: self)
            }
        case .ended, .cancelled, .failed:
            panGestureDidEnd()
        default: break
        }
    }
    
    private func panGestureDidEnd() {
        startAnimation()
    }
    
    // MARK: - private method

    private func setView(view: UIView) {
        reuseView?.removeFromSuperview()
        reuseView = nil
        shadowView.removeFromSuperview()
        centerView?.removeFromSuperview()
        addSubview(view)
        centerView = view
    }
    
    private func autoShowNextView() {
        pauseAnimation()
        if self.speed == 0 {
            return
        }
        if let centerView = centerView, let view = dataSource?.afterView(in: self, centerView: centerView) {
            var frame = view.frame
            frame.size.height = 0
            view.frame = frame
            view.clipsToBounds = true
            addSubview(view)
            reuseView = view
            
            view.addSubview(shadowView)
            shadowView.translatesAutoresizingMaskIntoConstraints = false

            let height = NSLayoutConstraint(item: shadowView, attribute: .height, relatedBy:.equal, toItem:nil, attribute: .height,multiplier: 1.0, constant: 2)
            shadowView.addConstraint(height)
            
            let leading = NSLayoutConstraint(item: shadowView, attribute: .leading, relatedBy:.equal, toItem:view, attribute: .leading,multiplier: 1.0, constant: 0)
            view.addConstraint(leading)
            
            let trailing = NSLayoutConstraint(item: shadowView, attribute: .trailing, relatedBy:.equal, toItem:view, attribute: .trailing,multiplier: 1.0, constant: 0)
            view.addConstraint(trailing)
            
            let bottom = NSLayoutConstraint(item: shadowView, attribute: .bottom, relatedBy:.equal, toItem:view, attribute: .bottom,multiplier: 1.0, constant: 0)
            view.addConstraint(bottom)
            startAnimation()
        }
    }

    private func startAnimation() {
        pauseAnimation()
        if let _ = reuseView, speed > 0 {
            timer = Timer.scheduledTimer(timeInterval: speed / 2, target: self, selector: #selector(updateAnimation), userInfo: nil, repeats: true)
        }
    }
    
    private func pauseAnimation() {
        timer?.invalidate()
        timer = nil
    }
    
    @objc func updateAnimation() {
        updateReuseViewFrame(translationY:0.5)
    }
    
    func updateReuseViewFrame(translationY:CGFloat) {
        guard let view = reuseView else {
            pauseAnimation()
            return
        }
        if view.frame.size.height >= bounds.size.height {
            pauseAnimation()
            if let reuseView = reuseView {
                setView(view: reuseView)
                autoShowNextView()
            }
        }
        else {
            var frame = view.frame
            frame.size.height = min(bounds.size.height, max(frame.height + translationY,0))
            view.frame = frame
        }
    }
    
    deinit {
        pauseAnimation()
    }
}
