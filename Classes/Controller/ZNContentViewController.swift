//
//  ZNContentViewController.swift
//  ZNReader
//
//  Created by Ink on 2018/11/14.
//

import UIKit

class ZNContentViewController: UIViewController {

    var isBackground: Bool = false
    var available: Bool = false
    var contentView: UIView

    
    init(contentView: UIView?, viewSize: CGSize, isBackground: Bool) {
        guard let contentView = contentView else {
            self.available = false
            self.contentView = UIView.init()
            super.init(nibName: nil, bundle: nil)
            return
        }

        
        if isBackground {
            contentView.transform = CGAffineTransform.init(scaleX: -1, y: 1)
        }
        self.available = true
        self.contentView = contentView
        self.isBackground = isBackground
        super.init(nibName: nil, bundle: nil)
        self.view.addSubview(contentView)
        contentView.frame = CGRect.init(origin: CGPoint.zero, size: viewSize)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

}
