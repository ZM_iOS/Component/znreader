//
//  ZNReaderConfig.swift
//  ZNReader
//
//  Created by Ink on 2018/11/7.
//

import UIKit

public class ZMParagraphStyle {
    
    public var style = NSMutableParagraphStyle()
    public var font = UIFont.systemFont(ofSize: 16)
    public var textColor = UIColor.black
    
    public var attributes: [NSAttributedString.Key : Any] {
        return [NSAttributedString.Key.font:font, NSAttributedString.Key.paragraphStyle:style, NSAttributedString.Key.foregroundColor: textColor]
    }
    
    init() {
        style.firstLineHeadIndent = 0
        style.minimumLineHeight = 0
        style.lineSpacing = 5
        style.paragraphSpacing = 10
        style.alignment = NSTextAlignment.justified
        style.baseWritingDirection = NSWritingDirection.leftToRight
        style.lineBreakMode = NSLineBreakMode.byWordWrapping
        style.baseWritingDirection = NSWritingDirection.natural
    }
}

public class ZNReaderConfig {
    
    public var textSize = UIScreen.main.bounds.size
    
    public var firstTitle = ZMParagraphStyle()
    public var secondTitle = ZMParagraphStyle()
    public var mainBody = ZMParagraphStyle()
    
    /// 首页顶部缩进
    public var spaceBeforeFirstTitle: CGFloat = 50
    
    public var spaceBetweenFirstTitleAndSecondTitle: CGFloat = 15.0
    public var spaceBetweenSecondTitleAndMainBody: CGFloat = 30.0
    
    /// 是否自动调整顶部缩进 (根据上一页最后一行调整，上下滚动翻页时可能用到)
    public var adjustsTopIndent: Bool = false

    /// 是否自动微调行间距
    public var adjustsLineHeightToFitView: Bool = true
    
    // MARK: paragraph
    public var paragraphSelectedColor: UIColor = UIColor.clear
    /// 段首标识
    public var paragraphPrefix: String = "\t"
    
    public init() {
        firstTitle.style.lineSpacing = 10
        firstTitle.style.paragraphSpacing = 22
        firstTitle.font = UIFont.systemFont(ofSize: 22)
        
        secondTitle.style.lineSpacing = 10
        secondTitle.style.paragraphSpacing = 20
        secondTitle.font = UIFont.systemFont(ofSize: 20)
        
        mainBody.style.lineSpacing = 10
        mainBody.style.paragraphSpacing = 20
        mainBody.font = UIFont.systemFont(ofSize: 15)
    }
}
