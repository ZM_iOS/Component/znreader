//
//  ZNReaderView.swift
//  ZNReader
//
//  Created by Ink on 2018/11/7.
//

import UIKit

open class ZNReaderView: UIView {
    
    private var paragraphTailViews = NSHashTable<UIView>.weakObjects()
    private var placeholderViews = NSHashTable<UIView>.weakObjects()
    
    public var paragraphTailView:((_ paragraphInfo:ZNReaderParagraphInfo,_ lastLineFrame:CGRect) -> UIView?)?
    public var paragraphSelected:((_ paragraphInfo:ZNReaderParagraphInfo,_ readerView: ZNReaderView) -> Void)?
    
    // 背面标示，需使用时配置
    public var isBackground = false
    
    public var longPress: UILongPressGestureRecognizer?
        
    public class func view(segment: ZNReaderSegment) -> ZNReaderView {
        let view: ZNReaderView = ZNReaderView(frame: CGRect(origin: CGPoint.zero, size: segment.size))
        view.contextData = segment
        return view
    }
        
    public var contextData: ZNReaderSegment? {
        didSet {
            placeholderViews.allObjects.forEach({$0.removeFromSuperview()})
            placeholderViews.removeAllObjects()
            renderParagraphMessage()
            DispatchQueue.main.async {[weak self] in
                self?.contextData?.placeholders?.forEach({[weak self] in
                    $0.readerView = self
                    if let view = $0.customView?($0) {
                        self?.placeholderViews.add(view)
                        self?.addSubview(view)
                    }
                })
            }
        }
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        contentNeedUpdate()
        
        addGestureRecognizer({
            let longPress = UILongPressGestureRecognizer(target: self, action: #selector(longHandler(longPress:)))
            self.longPress = longPress
            return longPress
        }())
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override open class var layerClass: AnyClass {
        return YYAsyncLayer.self
    }
    
    private var _backgroundColor: UIColor? = .clear
    open override var backgroundColor: UIColor? {
        get {
            return _backgroundColor
        }
        set {
            _backgroundColor = newValue
        }
    }
    
    @objc func longHandler(longPress:UILongPressGestureRecognizer) {
        let point = longPress.location(in: self)
        guard longPress.state == .began else {
            return
        }
        contextData?.paragraphInfos.forEach({
            if ($0.frame.contains(point)), $0.canSelected == true {
                paragraphSelected?($0,self)
                return
            }
        })
    }
    
    public func renderParagraphMessage() {
        
        paragraphTailViews.allObjects.forEach({$0.removeFromSuperview()})
        paragraphTailViews.removeAllObjects()
        
        guard let contextData = contextData else {
            return
        }
        
        DispatchQueue.main.async {
            contextData.lines.forEach({[weak self] in
                self?.paragraphTail(line: $0)
            })
        }
        
        contentNeedUpdate()
    }
    
    func paragraphTail(line:ZNLine) {
        
        guard line.endType == .paragraph,
            let paragraphInfo = contextData?.paragraphInfos.first(where: {$0.lines.last == line}),
            let view = paragraphTailView?(paragraphInfo,line.frame),
            let superview = superview else {
                return
        }
        
        let size = view.frame.size != .zero ? view.frame.size : CGSize(width: line.frame.height, height: line.frame.height)
        let origin = CGPoint(x: line.frame.minX + line.lineGlyphPathWidth, y: line.frame.origin.y + (line.frame.size.height / 2) - size.height / 2)
        
        view.frame = CGRect(origin: self.convert(origin, to: superview), size: size)
        paragraphTailViews.add(view)
        superview.addSubview(view)
    }
    
    func contentNeedUpdate() {
        layer.setNeedsDisplay()
    }
}

extension ZNReaderView: YYAsyncLayerDelegate {
    var newAsyncDisplayTask: YYAsyncLayerDisplayTask {
        let task = YYAsyncLayerDisplayTask()
        
        task.willDisplay = { layer in
            layer.isOpaque = false
            layer.contentsScale = UIScreen.main.scale
            layer.backgroundColor = self.backgroundColor?.cgColor
            layer.masksToBounds = false
        }
        
        task.display = {[weak self] context, size, isCancelled in
                   
            withExtendedLifetime(self) {
                
                guard isCancelled?() != true,
                    let contextData = self?.contextData ,
                      let config = contextData.config else  {
                    return
                }
                
                contextData.lines.forEach({
                    if let _ = $0.text,
                       let lineRef = $0.attributedString(config: config, needLine: true).lineRef {
                        let linePoint = CGPoint.init(x: $0.frame.origin.x, y: contextData.size.height - $0.frame.origin.y - $0.frame.size.height)
                        
                        self?.drawSelectedPath(line: $0,context: context)
                        context.textPosition = CGPoint(x: linePoint.x, y: linePoint.y + $0.lineDescent)
                        CTLineDraw(lineRef, context)
                    }
                })
            }
        }
    
        return task
    }
    
    func drawSelectedPath(line:ZNLine, context: CGContext) {
        guard let contextData = contextData,
            line.isSelected == true else {
            return
        }
        let linePoint = CGPoint.init(x: line.frame.origin.x, y: contextData.size.height - line.frame.origin.y - line.frame.size.height)
        let lineRect = CGRect(origin: CGPoint(x: linePoint.x + line.paragraphPrefixStartWidth, y: linePoint.y), size: CGSize(width: line.lineGlyphPathWidth - line.paragraphPrefixStartWidth, height: line.frame.size.height))
        let pathRef = CGMutablePath()
        contextData.config?.paragraphSelectedColor.setFill()
        pathRef.addRect(lineRect)
        context.addPath(pathRef)
        context.fillPath()
    }
}
