//
//  ZNReaderParser.swift
//  ZNReader
//
//  Created by Ink on 2018/11/7.
//

import UIKit

public class ZNReaderData: NSObject {
    public var firstTitle: String
    public var secondTitle: String
    public var mainBody: String
    
    public init(firstTitle: String?, secondTitle: String?, mainBody: String?) {
        self.firstTitle = firstTitle ?? ""
        self.secondTitle = secondTitle ?? ""
        self.mainBody = mainBody ?? ""
        super.init()
    }
}

let parserQueue: DispatchQueue = DispatchQueue.init(label: "znreader_parser")
let pageheightTolerate = 3

public class ZNReaderParser: NSObject {
    
    private let data: ZNReaderData
    private weak var readerConfig: ZNReaderConfig?
    private var fullLines = [ZNLine]()
    private var placeholdersCache = [Int: [ZNReaderPlaceholder]]()
    
    private var config: ZNReaderConfig {
        return readerConfig ?? ZNReaderConfig()
    }
    
    public init(data: ZNReaderData, config: ZNReaderConfig) {
        self.data = data
        self.readerConfig = config
        super.init()
    }
    
    public func parse(placeholderBlock: @escaping ((_ index: Int, _ totalCount: Int, _ segment: ZNReaderSegment) -> [ZNReaderPlaceholder]?),
                      drawData: @escaping (_ data: [ZNReaderSegment]) -> Void) {
        parseForward(placeholderBlock: placeholderBlock,drawData: drawData)
    }
    public func parseForward(placeholderBlock: @escaping ((_ index: Int, _ totalCount: Int, _ segment: ZNReaderSegment) -> [ZNReaderPlaceholder]?),
                             drawData: @escaping (_ data: [ZNReaderSegment]) -> Void) {
        parserQueue.async {
            let array = self.parseForward(placeholderBlock: placeholderBlock)
            DispatchQueue.main.async {
                drawData(array)
            }
        }
    }

    private func parseForward(placeholderBlock: @escaping ((_ index: Int, _ totalCount: Int, _ segment: ZNReaderSegment) -> [ZNReaderPlaceholder]?)) -> [ZNReaderSegment] {
        
        self.fullLines = createLines()
        
        var segments = parse(with: self.fullLines, index: 0, placeholderArray: nil)
        var index = 0
        while index < segments.count {
            guard let placeholders = placeholderBlock(index, segments.count, segments[index]) else {
                index += 1
                continue
            }
            placeholdersCache[index] = placeholders
            var remainLines = [ZNLine]()
            var i = index
            while i < segments.count {
                remainLines += segments[i].lines
                i += 1
            }
            segments.removeSubrange(index..<segments.count)
            segments += parse(with: remainLines, index: index, placeholderArray: placeholders)
            index += 1
        }
        parseParagrapth(segments: segments)
        return segments
    }
    
    private func parse(with remainLines: [ZNLine], index: Int,
                       placeholderArray: [ZNReaderPlaceholder]?) -> [ZNReaderSegment] {
        
        var lines = remainLines
        var segments = [ZNReaderSegment]()
        var placeholders = placeholderArray
        var indexVar = index
        
        while lines.count > 0 {
            let result = segment(with: lines, index: indexVar, placeholders: placeholders)
            lines = result.remainLines
            segments.append(result.segment)
            placeholders = nil
            indexVar += 1
        }
        
        return segments
    }
    
    private func segment(with lineArray: [ZNLine],
                         index: Int,
                         placeholders: [ZNReaderPlaceholder]?) -> (segment: ZNReaderSegment, remainLines: [ZNLine]) {
        
        let segment = ZNReaderSegment.init(index: index, config: self.config, placeholders: (placeholders != nil) ? placeholders : placeholdersCache[index])
        var lines = lineArray
        
        if config.adjustsTopIndent, let line = lineArray.first, line.endType != .none {
            if let index = self.fullLines.firstIndex(of: line), index != 0 {
                let prevLine = self.fullLines[index-1]
                switch prevLine.endType {
                case .line:
                    let insertLine = ZNLine.init(height: line.style.lineSpacing,lineType: .space)
                    insertLine.textRange = CFRangeMake(prevLine.textRange.location + prevLine.textRange.length, 0)
                    lines.insert(insertLine, at: 0)
                case .paragraph:
                    let insertLine = ZNLine.init(height: line.style.paragraphSpacing,lineType: .space)
                    insertLine.textRange = CFRangeMake(prevLine.textRange.location + prevLine.textRange.length, 0)
                    lines.insert(insertLine, at: 0)
                case .none: break
                }
            }
        }
        
        var result = true
        while (lines.count > 0 && result) {
            if let firstLine = lines.first {
                result = append(firstLine, to: segment)
            }
            if result {
                lines.remove(at: 0)
            }
        }
        
        guard let firstArray = segment.linesTemp.first, let firstLine = firstArray.first else {
            return (segment, lines)
        }
        guard let lastArray = segment.linesTemp.last, let lastLine = lastArray.last else {
            return (segment, lines)
        }
        
        segment.range = NSRange.init(location: firstLine.textRange.location,
                                     length: lastLine.textRange.location + lastLine.textRange.length - firstLine.textRange.location)
        
        if config.adjustsLineHeightToFitView && lines.count > 0 {
            if let placeholders = segment.placeholders, placeholders.count > 0 {
                var maxY: CGFloat = 0
                for p in placeholders {
                    maxY = p.y + p.height > maxY ? p.y + p.height : maxY
                }
                if let first = lastArray.first, first.frame.minY >= maxY {
                    let offsetY: CGFloat = segment.size.height - CGFloat(pageheightTolerate) - lastLine.frame.maxY
                    for line in lastArray {
                        line.frame = CGRect.init(origin: CGPoint.init(x: line.frame.origin.x, y: line.frame.origin.y+offsetY),size: line.frame.size)
                    }
                }
            }
            else {
                let lineNumber: Int = firstArray.count
                if lineNumber > 1 {
                    let offsetY: CGFloat = (segment.size.height - CGFloat(pageheightTolerate) - lastLine.frame.maxY)/CGFloat((lineNumber-1))
                    var i: Int = 0
                    for lineArray in segment.linesTemp {
                        for line in lineArray {
                            line.frame = CGRect.init(origin: CGPoint.init(x: line.frame.origin.x, y: line.frame.origin.y + offsetY * CGFloat(i)),size: line.frame.size)
                            i = i + 1
                        }
                    }
                }
            }
        }
        
        var array = [ZNLine]()
        for subArray in segment.linesTemp {
            array += subArray
        }
        segment.lines = array
        segment.linesTemp.removeAll()
        return (segment, lines)
    }
    
    private func append(_ line: ZNLine, to segment: ZNReaderSegment) -> Bool {
        var space: CGFloat = 0
        let pageWidth = segment.size.width
        let pageHeight = segment.size.height - CGFloat(pageheightTolerate)
        let lastLine: ZNLine? = segment.linesTemp.last?.last
        let lastLineMaxY: CGFloat = lastLine?.frame.maxY ?? 0
        
        if line.endType == .none {
            line.frame = CGRect.init(x: 0, y: lastLineMaxY, width: segment.size.width, height: line.height)
            if var array = segment.linesTemp.last {
                segment.linesTemp.removeLast()
                array.append(line)
                segment.linesTemp.append(array)
            }
            else {
                var array: [ZNLine] = [ZNLine]()
                array.append(line)
                segment.linesTemp.append(array)
            }
            return true
        }
        
        if let lastLine = lastLine {
            if lastLine.endType == .paragraph {
                space = space + line.style.paragraphSpacing
            } else if lastLine.endType == .line {
                space = space + line.style.lineSpacing
            }
        }
        
        var expectFrame = CGRect.init(x: 0, y: lastLineMaxY + space, width: pageWidth, height: line.height)
        
        if expectFrame.minY > pageHeight || expectFrame.maxY > pageHeight { return false }
        
        var needNewArray = false
        if let array = segment.placeholders {
            for placeholder in array {
                if ((expectFrame.minY > placeholder.y && expectFrame.minY < placeholder.y + placeholder.height) ||
                    (expectFrame.maxY > placeholder.y && expectFrame.maxY < placeholder.y + placeholder.height)) {
                    expectFrame = CGRect.init(x: 0, y: placeholder.y + placeholder.height, width: pageWidth, height: line.height)
                    needNewArray = true
                }
            }
        }
        
        if expectFrame.minY > pageHeight || expectFrame.maxY > pageHeight { return false }
        line.frame = expectFrame
        
        if needNewArray {
            var array = [ZNLine]()
            array.append(line)
            segment.linesTemp.append(array)
            return true
        }
        
        if var array = segment.linesTemp.last {
            segment.linesTemp.removeLast()
            array.append(line)
            segment.linesTemp.append(array)
            return true
        }
        else  {
            var array = [ZNLine]()
            array.append(line)
            segment.linesTemp.append(array)
            return true
        }
    }
    
    // MARK: -
    
    private func createLines() -> [ZNLine] {
        
        func getLines(range:NSRange,parseLines:[CTLine]) -> [CTLine] {
            return parseLines.filter({
                let lineContentRange = CTLineGetStringRange($0)
                return lineContentRange.location >= range.location && (lineContentRange.location + lineContentRange.length <= range.location + range.length)
            })
        }
        
        let attString = NSMutableAttributedString()
        attString.append(NSAttributedString(string: data.firstTitle, attributes: config.firstTitle.attributes))
        attString.append(NSAttributedString(string: data.secondTitle, attributes: config.secondTitle.attributes))
        attString.append(NSAttributedString(string: data.mainBody, attributes: config.mainBody.attributes))
        
        let parseLines = getLinesWithAttributeString(attString: attString, width: config.textSize.width)
        
        var lines: [ZNLine] = [ZNLine]()
        
        let firstTitleRange = NSRange(location: 0, length: data.firstTitle.count)
        let firstTitleLines: [CTLine] = getLines(range: firstTitleRange, parseLines: parseLines)
        if firstTitleLines.count > 0 {
            lines.append(ZNLine.init(height: self.config.spaceBeforeFirstTitle,lineType:.space))
            lines = merge(ctLinearray: firstTitleLines, znLineArray: lines, string: attString.string,style: config.firstTitle.style,lineType: .firstTitle)
        }
        
        let secondTitleRange = NSRange(location: firstTitleRange.location + firstTitleRange.length, length: data.secondTitle.count)
        let secondTitleLines: [CTLine] = getLines(range: secondTitleRange, parseLines: parseLines)
        if secondTitleLines.count > 0 {
            lines.append(ZNLine.init(height: self.config.spaceBetweenFirstTitleAndSecondTitle,lineType:.space))
            lines = merge(ctLinearray: secondTitleLines, znLineArray: lines, string: attString.string, style: config.secondTitle.style,lineType: .secondTitle)
        }
        
        let mainBodyRange = NSRange(location: secondTitleRange.location + secondTitleRange.length, length: data.mainBody.count)
        let mainBodyLines: [CTLine] = getLines(range: mainBodyRange, parseLines: parseLines)
        if mainBodyLines.count > 0 {
            lines.append(ZNLine.init(height: config.spaceBetweenSecondTitleAndMainBody,lineType:.space))
            lines = merge(ctLinearray: mainBodyLines, znLineArray: lines, string: attString.string,style: config.mainBody.style,lineType: .content)
        }
        
        return lines
    }
    
    private func merge(ctLinearray: [CTLine],
                       znLineArray: [ZNLine],
                       string: String,
                       style:NSParagraphStyle,
                       lineType:ZNLineType = .none) -> [ZNLine] {
        var array = znLineArray
        for lineRef in ctLinearray {
            autoreleasepool {
                let range = CTLineGetStringRange(lineRef)
                let height = CTLineGetBoundsWithOptions(lineRef, .useOpticalBounds).size.height
                let text = (string as NSString).substring(with: NSRange(location: range.location, length: range.length)) as String
                
                var lineDescent = CGFloat()
                let lineWidth = CTLineGetTypographicBounds(lineRef, nil, &lineDescent, nil)
                var xStart = CTLineGetOffsetForStringIndex(lineRef, 0, nil)
                let paragraphStartTxt = self.config.paragraphPrefix
                
                if text.hasPrefix(paragraphStartTxt) {
                    xStart = CTLineGetOffsetForStringIndex(lineRef, range.location + paragraphStartTxt.count, nil)
                }
                
                let endType:ZNLineEndType = (text.hasSuffix("\n") || lineRef ==  ctLinearray.last) ? .paragraph : .line
                array.append(ZNLine.init(range: range,
                                         text: String(text),
                                         height: height,
                                         lineDescent: lineDescent,
                                         lineGlyphPathWidth: CGFloat(lineWidth),
                                         paragraphPrefixStartWidth:xStart,
                                         endType: endType,
                                         style: style,
                                         lineType:lineType))
            }
        }
        return array
    }
    
    // MARK: - privite
    
    private func getLinesWithAttributeString(attString: NSAttributedString, width: CGFloat) -> [CTLine] {
        let path = CGMutablePath()
        let pathRect = CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude)
        path.addRect(pathRect)
        
        let frameSetter = CTFramesetterCreateWithAttributedString(attString)
        let frame = CTFramesetterCreateFrame(frameSetter, CFRangeMake(0, attString.length), path, nil)
        
        let lines = CTFrameGetLines(frame)
        return lines as? [CTLine] ?? [CTLine]()
    }
    
}

extension ZNReaderParser {
    
    private func parseParagrapth(segments:[ZNReaderSegment]) {
        
        var paragrapthIndex = 0
        
        segments.forEach({
            let result = paragraphs(with: $0, paragraphIndex: paragrapthIndex)
            paragrapthIndex = result.nextParagraphIndex
            $0.paragraphInfos = result.paragraphs
        })
        
        let paragraphInfos = segments.flatMap({$0.paragraphInfos})
        Dictionary(grouping: paragraphInfos, by: { $0.paragraphIndex }).forEach({item in
            item.value.forEach({paragraphInfo in
                paragraphInfo.contextFullText = item.value.reduce("", {$0 + $1.contentText})
            })
        })
    }
    
    private func paragraphs(with segment: ZNReaderSegment,
                            paragraphIndex: Int) -> (paragraphs: [ZNReaderParagraphInfo],nextParagraphIndex:Int) {
        
        var newParagraphIndex = paragraphIndex
        var paragraphInfos = [ZNReaderParagraphInfo]()
        var paragraphLineInfos = [ZNLine]()
        
        segment.lines.forEach({
            if $0.lineType == .content && $0.textRange.length > 0 && $0.text != "\n"{
                paragraphLineInfos.append($0)
                if $0 == segment.lines.last || $0.endType == .paragraph {
                    let paragraphInfo = ZNReaderParagraphInfo()
                    paragraphInfo.lines = paragraphLineInfos
                    paragraphInfo.paragraphIndex = newParagraphIndex
                    paragraphInfo.frame = CGRect(origin: CGPoint(x: 0, y: paragraphLineInfos.first?.frame.origin.y ?? 0),size: CGSize(width: config.textSize.width, height: (paragraphLineInfos.last?.frame.maxY  ?? 0) - (paragraphLineInfos.first?.frame.minY ?? 0)))
                    paragraphInfos.append(paragraphInfo)
                    
                    newParagraphIndex += ($0.endType == .paragraph ? 1 : 0)
                    paragraphLineInfos = [ZNLine]()
                }
            }
        })
        
        return (paragraphInfos, newParagraphIndex)
    }
    
}
