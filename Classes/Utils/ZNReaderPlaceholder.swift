//
//  ZNReaderPlaceholder.swift
//  ZNReader
//
//  Created by Ink on 2018/11/17.
//

import UIKit

public class ZNReaderPlaceholder: NSObject {
    
    public var y: CGFloat
    public var height: CGFloat
    public weak var readerView:ZNReaderView?
    
    public var customView:((_ placeholder:ZNReaderPlaceholder) -> UIView?)?
    
    public init(originY y: CGFloat, height: CGFloat, customView: ((_ placeholder:ZNReaderPlaceholder) -> UIView?)? = nil) {
        self.y = y
        self.height = height
        self.customView = customView
        super.init()
    }
}
