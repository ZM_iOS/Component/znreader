//
//  ZNReaderSegment.swift
//  ZNReader
//
//  Created by Ink on 2018/11/7.
//

import UIKit

enum ZNLineEndType: Int {
    case none
    // 行
    case line
    // 段尾 （/n）
    case paragraph
}

// 行类型
enum ZNLineType: Int {
    case none
    // 间距 占位
    case space
    // 标题
    case firstTitle
    // 副标题
    case secondTitle
    // 内容
    case content
}

class ZNLine: NSObject {
    
    var lineType: ZNLineType = .none
    var endType: ZNLineEndType = .none
    var height: CGFloat = 0
    
    var textRange: CFRange = CFRangeMake(0, 0)
    var text: String?
    // 额外的富文本信息
    public var extraAttributes = [(attributes: [NSAttributedString.Key : Any],range: NSRange)]()
    
    /// 选中状态
    var isSelected = false
        
    var style = NSParagraphStyle()
    
    var lineDescent: CGFloat = 0
    var lineGlyphPathWidth : CGFloat = 0
    var paragraphPrefixStartWidth : CGFloat = 0
    
    init(height: CGFloat,lineType:ZNLineType) {
        self.height = height
        self.lineType = lineType
        super.init()
    }
    
    init(range: CFRange,
         text: String? = nil,
         height: CGFloat,
         lineDescent: CGFloat,
         lineGlyphPathWidth: CGFloat,
         paragraphPrefixStartWidth:CGFloat,
         endType: ZNLineEndType,
         style: NSParagraphStyle,
         lineType:ZNLineType) {
        self.textRange = range
        self.text = text
        self.height = height
        self.lineDescent = lineDescent
        self.lineGlyphPathWidth = lineGlyphPathWidth
        self.paragraphPrefixStartWidth = paragraphPrefixStartWidth
        self.endType = endType
        self.style = style
        self.lineType = lineType
        super.init()
    }
    
    var frame: CGRect = CGRect.zero
    
    public func attributedString(config:ZNReaderConfig?,needLine: Bool = false) -> (attributedString:NSAttributedString?,lineRef:CTLine?) {
        guard let string = text, let config = config else {
            return (nil,nil)
        }
        let attributes:[NSAttributedString.Key : Any]
        switch lineType {
        case .firstTitle:
            attributes = config.firstTitle.attributes
        case .secondTitle:
            attributes = config.secondTitle.attributes
        default:
            attributes = config.mainBody.attributes
        }
        let attributedString = NSMutableAttributedString(string: string, attributes: attributes)
        for extra in extraAttributes {
            attributedString.addAttributes(extra.attributes, range: extra.range)
        }
        
        var lineRef: CTLine?
        if needLine == true {
            let newCtLine = CTLineCreateWithAttributedString(attributedString)
            lineRef = endType == .line ? (CTLineCreateJustifiedLine(newCtLine, 1.0, Double(frame.size.width)) ?? newCtLine) : newCtLine
        }
        
        return (attributedString,lineRef)
    }
}

/// 页内的分段信息
public class ZNReaderParagraphInfo: NSObject {
    
    public var paragraphIndex: Int = -1
    
    var lines: [ZNLine] = [ZNLine]()
    /// 是否可选中
    public var canSelected = false
    /// 选中状态
    public var isSelected = false {
        didSet {
            lines.forEach({$0.isSelected = isSelected})
        }
    }

    public var frame = CGRect.zero
    
    /// 该段在当前分页内的内容 在 当前章节的内容 range
    public var range: NSRange = NSRange.init(location: 0, length: 0)
    /// 关联上下文 该段完整的 内容
    public var contextFullText: String?
    /// 分段文字
    public var contentText: String {
        return lines.map({$0.text ?? ""}).reduce(String()){$0 + $1 }
    }
}

/// 分页信息
public class ZNReaderSegment: NSObject {

    public var index: Int

    /// 分页时候使用，用于临时存储
    var linesTemp = [[ZNLine]]()
    var lines = [ZNLine]()
    
    public weak var config: ZNReaderConfig?
    
    /// 分段信息
    public var paragraphInfos: [ZNReaderParagraphInfo] = [ZNReaderParagraphInfo]()
    
    /// 分页文字
    public var contentText: String {
        return lines.map({$0.text ?? ""}).reduce(String()){$0 + $1 }
    }
    
    public var size: CGSize
    /// 一页文字 range
    public var range: NSRange = NSRange.init(location: 0, length: 0)
    
    public var placeholders: [ZNReaderPlaceholder]?
    
    public var spaceFrame: CGRect {
        get {
            let lastLineFrame = lines.last?.frame ?? CGRect.zero
            return CGRect.init(x: 0, y: lastLineFrame.maxY, width: size.width, height: size.height - lastLineFrame.maxY)
        }
    }
    
    public init(index: Int, config: ZNReaderConfig?, placeholders: [ZNReaderPlaceholder]?) {
        self.index = index
        self.config = config
        self.size = config?.textSize ?? .zero
        self.placeholders = placeholders
        super.init()
    }
    
    /// 页面移除附属富文本属性
    @discardableResult
    public func removeExtraAttributes() -> Self {
        lines.forEach({$0.extraAttributes.removeAll()})
        return self
    }
    
    /// 页面添加附属富文本属性（阴影，背景色，下划线等)
    @discardableResult
    public func appendagesAttributes(_ attrs: [NSAttributedString.Key : Any] = [:], range: NSRange) -> Self {
        
        var offset = 0, lineLocation = 0
        
        for line in lines {
            
            if  let _ = line.text,
                range.location <= lineLocation + line.textRange.length,
                range.location + range.length >= lineLocation{
                
                let relativeLocation = max(range.location - lineLocation, 0)
                let relativeLength = min(range.length - offset, line.textRange.length - relativeLocation)
                
                line.extraAttributes += [(attributes: attrs,range: NSRange(location: relativeLocation, length: relativeLength))]
                offset += relativeLength
                
                if offset > range.length { break }
            }
            
            lineLocation += line.textRange.length
        }
        return self
    }
}
