//
//  ViewController.swift
//  Demo
//
//  Created by Ink on 2018/11/7.
//  Copyright © 2018 ncnk. All rights reserved.
//

import UIKit
import ZNReader

let isFullScreen = (UIApplication.shared.statusBarFrame.size.height>20.0)

///导航栏高度
let kSafeAreaTopHeight:CGFloat = (isFullScreen ? 88 : 64)

///底部安全区域高度
let kSafeAreaBottomHeight:CGFloat = (isFullScreen ? 34 : 0)

///状态栏高度
let kStatusBarHeight:CGFloat = (isFullScreen ? 44 : 20)

///TabBar高度
let kTabBarHeight:CGFloat    = (isFullScreen ? 83 : 49)

let LFEndViewHeight: CGFloat = 600

let topOffset: CGFloat = kStatusBarHeight + 44 + 20
let leftOffset: CGFloat = 20
let bottomOffset: CGFloat = kSafeAreaBottomHeight + 20
let rightOffset: CGFloat = 20
let readerContentSize: CGSize = CGSize(width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height - topOffset - bottomOffset)

class ViewController: UIViewController, ZNPageViewControllerDataSource, ZNPageViewControllerDelegate {
    
    
    var pageVC: ZNPageViewController = ZNPageViewController.init(transitionStyle: .pageCurl)
    
    let readerConfig = ZNReaderConfig()
    
    var segments: [ZNReaderSegment]?
    lazy var transitionStyleButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.frame = CGRect(origin: CGPoint(x: UIScreen.main.bounds.size.width / 2 - 30, y: topOffset), size: CGSize(width: 60, height: 40))
        btn.backgroundColor = .red
        btn.addTarget(self, action: #selector(transitionStyleChange), for: .touchUpInside)
        view.addSubview(btn)
        return btn
    }()
    
    lazy var testButton: UIButton = {
        let btn = UIButton(type: .system)
        var frame = transitionStyleButton.frame
        frame.origin.y += transitionStyleButton.bounds.height + 2
        btn.frame = frame
        btn.backgroundColor = .red
        btn.addTarget(self, action: #selector(test), for: .touchUpInside)
        btn.setTitle("朗读", for: .normal)
        return btn
    }()
    
    lazy var testButton1: UIButton = {
        let btn = UIButton(type: .system)
        var frame = testButton.frame
        frame.origin.y += testButton.bounds.height + 2
        btn.frame = frame
        btn.backgroundColor = .red
        btn.addTarget(self, action: #selector(test1), for: .touchUpInside)
        btn.setTitle("继续", for: .normal)
        return btn
    }()
    
    weak var weakCurrenView: UIView?
    
    private var tapGestureRecognizer: UITapGestureRecognizer = UITapGestureRecognizer.init()
    
    func reload() {
        
        switch staticTransitionStyle {
        case .verticalScroll:
            pageVC.view.frame = CGRect(origin: CGPoint.init(x: 0, y: topOffset), size: readerContentSize)
        default:
            pageVC.view.frame = self.view.bounds
        }
        
        pageVC.transitionStyle = staticTransitionStyle
        
        if let view = contentView(weakCurrenView?.tag ?? 0) {
            pageVC.setView(view: view, direction: .forward, animated: false)
        }
        
        pageVC.transitionStyle == .autoGradually ? pageVC.speed = 0.046 : nil
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        testZNReader()
        
        self.tapGestureRecognizer.addTarget(self, action: #selector(tapGestureRecognizerAction(tapGestureRecognizer:)))
        pageVC.view.addGestureRecognizer(self.tapGestureRecognizer)
        
        NotificationCenter.default.addObserver(self, selector: #selector(paragraphCancelSelected), name: UIMenuController.willHideMenuNotification, object: nil)
    }
    
    var staticTransitionStyle: TransitionStyle = .autoGradually
    
    @objc func transitionStyleChange() { // 切换翻页模式
        staticTransitionStyle = TransitionStyle(rawValue: (staticTransitionStyle.rawValue + 1)%(TransitionStyle.autoGradually.rawValue + 1)) ?? .pageCurl
        reload()
        
        switch staticTransitionStyle {
        case .pageCurl:
            transitionStyleButton.setTitle("仿真", for: .normal)
        case .horizontalScroll:
            transitionStyleButton.setTitle("横向", for: .normal)
        case .verticalScroll:
            transitionStyleButton.setTitle("纵向", for: .normal)
        case .horizontalPageScroll:
            transitionStyleButton.setTitle("横向页", for: .normal)
        case .verticalPageScroll:
            transitionStyleButton.setTitle("纵向页", for: .normal)
        case .horizontalCover:
            transitionStyleButton.setTitle("横覆盖", for: .normal)
        case .verticalCover:
            transitionStyleButton.setTitle("纵覆盖", for: .normal)
        case .noneAnimation:
            transitionStyleButton.setTitle("无动画", for: .normal)
        case .noneAnimation_dragForbid:
            transitionStyleButton.setTitle("无动画_禁止拖拽", for: .normal)
        case .autoGradually:
            transitionStyleButton.setTitle("自动", for: .normal)
        }
    }
    
    var stackRange = NSRange()
    @objc func test() { // 模拟有声
        guard let currentView = weakCurrenView?.subviews.filter({$0.isKind(of: ZNReaderView.self)}).first as? ZNReaderView else {
            return
        }
        stackRange.length += 1
        currentView.contextData?.removeExtraAttributes()
        currentView.contextData = currentView.contextData?.appendagesAttributes([.foregroundColor: UIColor.red], range: stackRange)
    }
    
    @objc func test1() { // 暂停
        if pageVC.speed == 0 {
            // 每分钟 N 行
            let a = [10,33,48,60,100]
            let lineHeight = (readerConfig.mainBody.font.pointSize + readerConfig.mainBody.style.lineSpacing)
            let speedType = a[(Int(arc4random()) % a.count)]
            pageVC.speed = TimeInterval(60.0 / (lineHeight * CGFloat(speedType)))
        }
        else {
            pageVC.speed = 0
        }
    }
    
    @objc func tapGestureRecognizerAction(tapGestureRecognizer: UITapGestureRecognizer) {
        let point = tapGestureRecognizer.location(in: self.view)
        let oneThirdOfWidth = self.view.bounds.size.width/3.0
        if point.x < oneThirdOfWidth { // 左侧点击
            if let centerView = pageVC.shownViews.last, let view = beforeView(in: pageVC, view: centerView) {
                pageVC.setView(view: view, direction: .reverse, animated: true)
            }
        }
        else if point.x > oneThirdOfWidth*2 { // 右侧点击
            if let centerView = pageVC.shownViews.last, let view = afterView(in: pageVC, view: centerView) {
                pageVC.setView(view: view, direction: .forward, animated: true)
            }
        }
    }

    func testZNReader() {
        let testData = TextData.testData
        let data = ZNReaderData.init(firstTitle: testData.0, secondTitle: testData.1, mainBody: testData.2)
        readerConfig.firstTitle.font = UIFont.systemFont(ofSize: 30)
        readerConfig.mainBody.font = UIFont.systemFont(ofSize: 17)
        readerConfig.mainBody.textColor = UIColor.gray
        readerConfig.adjustsTopIndent = true
        readerConfig.textSize = CGSize.init(width: UIScreen.main.bounds.size.width - leftOffset - rightOffset,height: readerContentSize.height)
        readerConfig.paragraphSelectedColor = .lightGray
        let parser = ZNReaderParser.init(data: data, config: readerConfig)

        var needNewPage = false
        parser.parseForward(placeholderBlock: { (index, total, segment) -> [ZNReaderPlaceholder]? in
            if index == total - 1 {
                if segment.spaceFrame.origin.y + LFEndViewHeight <= readerContentSize.height {
                    let placeholder = ZNReaderPlaceholder.init(originY: segment.spaceFrame.origin.y, height: LFEndViewHeight) {placeholder in
                        let view  = UIView(frame: CGRect(origin: CGPoint(x: 20, y: placeholder.y + 10), size: CGSize(width: 200, height: LFEndViewHeight - 10)))
                        view.backgroundColor = UIColor.red
                        return view
                    }
                    return [placeholder]
                }
                else {
                    needNewPage = true
                }
            }
            return nil
        }) { [weak self] (segments) in
            segments.forEach({ $0.paragraphInfos.forEach({$0.canSelected = true})})
            self?.segments = segments
            if needNewPage == true , var newSegments = self?.segments{
                let placeholder = ZNReaderPlaceholder.init(originY: 0, height: LFEndViewHeight) {placeholder in
                    let view  = UIView(frame: CGRect(origin: CGPoint(x: 20, y: placeholder.y + 10), size: CGSize(width: 200, height: LFEndViewHeight - 10)))
                    view.backgroundColor = UIColor.red
                    return view
                }
                newSegments += [ZNReaderSegment(index: segments.count, config: self?.readerConfig, placeholders: [placeholder])]
                self?.segments = newSegments
            }
            self?.transitionStyleChange()
        }
        self.testPageVC()
        view.addSubview(self.testButton)
        view.addSubview(self.testButton1)
    }
    
    func testPageVC() {
        
        addChild(pageVC)
        view.addSubview(pageVC.view)
        pageVC.view.frame = self.view.bounds
        pageVC.dataSource = self
        pageVC.delegate = self
        pageVC.isDoubleSided = true
    }

    func beforeView(in pageViewController: ZNPageViewController, view centerView: UIView) -> UIView? {
        return contentView(centerView.tag-1)
    }
    
    func afterView(in pageViewController: ZNPageViewController, view centerView: UIView) -> UIView? {
        return contentView(centerView.tag+1)
    }
    
    func backgroundView(in pageViewController: ZNPageViewController, for view: UIView) -> UIView? {
        return contentView(view.tag, isBackground: true)
    }
    
    func currentView(in pageViewController: ZNPageViewController, didChangeTo view: UIView) {
        print("当前 \(view.tag)")
        stackRange = NSRange(location: 5, length: 0)
        weakCurrenView = view
    }
    
    private func contentView(_ forPage: NSInteger?, isBackground:Bool = false) -> UIView? {
        
        guard let segments =  segments, let page = forPage, page >= 0 ,page < segments.count else {
            return nil
        }
        
        let segment = segments[page]
        let view = UIView(frame: self.view.bounds)
        // 背景色
        view.backgroundColor = UIColor.white
        view.tag = page
        let textView = ZNReaderView.view(segment: segment)
        view.addSubview(textView)
        textView.paragraphTailView = {[weak self](paragraph,lastLineFrame) in
            let btn = UIButton()
            btn.setTitle("\(paragraph.paragraphIndex)", for: .normal)
            btn.addTarget(self, action: #selector(self?.btnHandler), for: .touchUpInside)
            btn.backgroundColor = UIColor.red
            return btn
        }
        
        textView.paragraphSelected = {[weak self] (paragraph,readerView) in
            guard let self = self else {return}
            
            self.segments?.forEach({$0.paragraphInfos.forEach({$0.isSelected = $0.paragraphIndex == paragraph.paragraphIndex})})
            self.pageVC.shownViews.forEach { view in
                view.subviews.filter({$0 is ZNReaderView}).forEach({
                    if let readView = $0 as? ZNReaderView {
                        let segment = readView.contextData
                        readView.contextData = segment
                    }
                })
            }
            
            var menuItems = [UIMenuItem]()

            menuItems.append(UIMenuItem(title: "评论", action: #selector(ViewController.commandAction)))
            menuItems.append(UIMenuItem(title: "分享", action: #selector(ViewController.shareAction)))
            let menu = UIMenuController.shared
            menu.menuItems = menuItems
            menu.setTargetRect(paragraph.frame, in: readerView)
            UIMenuController.shared.setMenuVisible(true, animated: true)
        }

        textView.frame = CGRect.init(origin: CGPoint.init(x: leftOffset, y: topOffset), size: readerConfig.textSize)

        if pageVC.transitionStyle == .verticalScroll {
            var frame = textView.frame;
            frame.origin.y = 0
            textView.frame = frame
        }
        return view
    }
    
    @objc func btnHandler() {
        print("dasdasdasdasd")
    }
    
    @objc func shareAction() {
        commandAction()
    }
    
    @objc func commandAction() {
        let selectedParagraph = segments?.flatMap({$0.paragraphInfos}).first(where: {$0.isSelected == true})
        print(selectedParagraph?.contextFullText ?? "")
        paragraphCancelSelected()
    }
    
    @objc func paragraphCancelSelected() {
        segments?.forEach({$0.paragraphInfos.forEach({$0.isSelected = false})})
        pageVC.shownViews.forEach { view in
            view.subviews.filter({$0 is ZNReaderView}).forEach({
                if let readView = $0 as? ZNReaderView {
                    let segment = readView.contextData
                    readView.contextData = segment
                }
            })
        }
    }
    
    open override var canBecomeFirstResponder: Bool {
        return true
    }
    
    open override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        return UIMenuController.shared.menuItems?.first(where: {$0.action == action}) != nil
    }
    
    deinit {
        print("deinit class name = ")
    }
}

